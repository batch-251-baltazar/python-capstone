# 1. Create a Person class that is an abstract class that has the following methods:
# a. getFullName method
# b. addRequest method
# c. checkRequest method
# d. addUser method

from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self, firstName, lastName):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

# 2. Create an Employee class from Person with the following properties and methods:
	# a. Properties (Make sure they are private and have getters/setters)
		# i. firstName
		# ii. lastName
		# iii. email
		# iv. department
	# b. Methods
	# c. Abstract methods (All methods just return Strings of simple text)
		# i. checkRequest() - placeholder method
		# ii. addUser() - placeholder method
		# iii. login() - outputs "<Email> has logged in"
		# iv. logout() - outputs "<Email> has logged out"


class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# getters of Employee class

	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department


	# setters of Employee class

	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = email

	def set_department(self):
		self._department = department

	# implementing the abstract methods from the parent class

	def getFullName(self, firstName, lastName):
		print(f"{firstName} {lastName}")

	def checkRequest(self):
		print()

	def addUser(self):
		print()

	def login(self, email):
		print(f"{email} has logged in")

	def logout(self, email):
		print(f"{email} has logged out")

# 3. Create a TeamLead class from Person with the following properties and methods:
# a. Properties (Make sure they are private and have getters/setters)
		# i. firstName
		# ii. lastName
		# iii. email
		# iv. department
	# b. Methods
	# c. Abstract methods (All methods just return Strings of simple text)
		# i. checkRequest() - placeholder method
		# ii. addUser() - placeholder method
		# iii. login() - outputs "<Email> has logged in"
		# iv. logout() - outputs "<Email> has logged out"
		# v. addMember() - adds an employee to the members list

class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# getters of TeamLead class

	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department


	# setters of TeamLead class

	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = email

	def set_department(self):
		self._department = department

	# implementing the abstract methods from the parent class

	def getFullName(self, firstName, lastName):
		print(f"{firstName} {lastName}")

	def checkRequest(self):
		print()

	def addUser(self):
		print()

	def login(self, email):
		print(f"{email} has logged in")

	def logout(self, email):
		print(f"{email} has logged out")

	def addMember(self, name):
		print()


# 4. Create an Admin class from Person with the following properties and methods:
# a. Properties (Make sure they are private and have getters/setters)
		# i. firstName
		# ii. lastName
		# iii. email
		# iv. department
	# b. Methods
	# c. Abstract methods (All methods just return Strings of simple text)
		# i. checkRequest() - placeholder method
		# ii. addUser() - placeholder method
		# iii. login() - outputs "<Email> has logged in"
		# iv. logout() - outputs "<Email> has logged out"
		# v. addUser() - outputs "New user added"

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# getters of Admin class

	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department


	# setters of Admin class

	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = email

	def set_department(self):
		self._department = department

	# implementing the abstract methods from the parent class

	def getFullName(self, firstName, lastName):
		print(f"{firstName} {lastName}")

	def checkRequest(self):
		print()

	def addUser(self):
		print()

	def login(self, email):
		print(f"{email} has logged in")

	def logout(self, email):
		print(f"{email} has logged out")

	def addUser(self):
		print("New user added")


# 5. Create a Request class that has the following properties and methods:
# a. Properties
		# i. name
		# ii. requester
		# iii. dateRequested
		# iv. status
	# b. Methods
		# i. updateRequest
		# ii. closeRequest
		# iii. cancelRequest

class Request():
	def __init__(self, name, requester, dateRequested, status):
		super().__init__()
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = status

	# getters of Request class

	def get_name(self):
		return self._name

	def get_requester(self):
		return self._requester

	def get_dateRequested(self):
		return self._dateRequested

	def get_status(self):
		return self._status


	# setters of Request class

	def set_firstName(self):
		self._name = name

	def set_requester(self):
		self._requester = requester

	def set_dateRequested(self):
		self._dateRequested = dateRequested

	def set_status(self):
		self._status = status

	# methods

	def updateRequest(self):
		print()

	def closeRequest(self,name, status):
		print(f"{name} has been {status}")

	def cancelRequest(self):
		print()

# Capstone Test Cases

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee3.getFullName("Robert", "Patterson")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
employee3.getFullName("Brandon", "Smith")
req2 = Request("Laptop repair", employee1, "1-Jul-2021", "closed")
req2.closeRequest("Laptop repair","closed")

